import mongoose from 'mongoose';
const RentListingSchema = new mongoose.Schema({
	title: {
		type: String,
		trim: true,
		required: 'Title is required'
	},
	category: {
		/* type: Enumerator, */
		type: String,
		trim: true,
		required: true
	},
	short_address: {
		type: String,
		trim: true,
		required: true
	},
	long_address: {
		type: String,
		trim: true,
		required: true
	},
	rate_monthly: {
		type: Number,
		trim: true,
		required: true
	},
	conditions: {
		type: String,
		trim: true,
		required: true
	},
	status: {
		type: Boolean,
		trim: true,
		required: true
	},
	interested_people: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
	images: [
		{
			data: Buffer,
			contentType: String
		}
	],
	image360: {
		data: Buffer,
		contentType: String
	},
	description: {
		type: String,
		trim: true
	},
	updated: Date,
	created: {
		type: Date,
		default: Date.now
	},
	renter: { type: mongoose.Schema.ObjectId, ref: 'User' }
});

RentListingSchema.index({ description: 'text', title: 'text', category: 'text', short_address: 'text' });

//should I rearrange to make more sense?

export default mongoose.model('RentListing', RentListingSchema);
