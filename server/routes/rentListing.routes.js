import express from 'express';
import rentListingCtrl from '../controllers/rentListing.controller';
import userCtrl from '../controllers/user.controller';
import authCtrl from '../controllers/auth.controller';

const router = express.Router();

router
	.route('/api/rentListings')
	.get(rentListingCtrl.list)
	.post(authCtrl.requireSignin, rentListingCtrl.create);

router.route('/api/rentListings/showInterest/:rentListingId').put(authCtrl.requireSignin, rentListingCtrl.showInterest);

router.route('/api/rentListings/generalSearch').post(rentListingCtrl.generalSearch);

router.route('/api/rentListings/specificSearch').post(rentListingCtrl.specificSearch);

router.route('/api/rentListings/getDistinctShort_address').get(rentListingCtrl.getDistinctShort_address);

//keep the routes requiring params in the end

router.route('/api/rentListings/user/:userId').get(authCtrl.requireSignin, rentListingCtrl.rentListingsByUserId);

router
	.route('/api/rentListings/:rentListingId')
	.get(rentListingCtrl.read)
	.put(authCtrl.requireSignin, rentListingCtrl.isRenter, rentListingCtrl.update)
	.delete(authCtrl.requireSignin, rentListingCtrl.isRenter, rentListingCtrl.remove);

router.param('userId', userCtrl.userByID);

router.param('rentListingId', rentListingCtrl.rentListingById);

export default router;
