import RentListing from '../models/rentListing.model';
import _ from 'lodash';
import errorHandler from './../helpers/dbErrorHandler';
import { json } from 'body-parser';

const removeDuplicates = (arr, key) => {
	var newArray = [];
	var lookupObject = {};

	for (var i in arr) {
		lookupObject[arr[i][key]] = arr[i];
	}

	for (i in lookupObject) {
		newArray.push(lookupObject[i]);
	}
	return newArray;
};

const isRenter = (req, res, next) => {
	const authorized = req.rentListing && req.auth && req.rentListing.renter._id == req.auth._id;
	if (!authorized) {
		return res.status('403').json({
			error: 'User is not authorized'
		});
	}
	next();
};

//create rent listing
const create = async (req, res) => {
	const rentListing = new RentListing(req.body);
	rentListing.renter = req.auth._id;
	try {
		await rentListing.save();
		return res.status(200).json({
			message: 'Item set for Rent!'
		});
	} catch (err) {
		return res.status(400).json({
			error: errorHandler.getErrorMessage(err)
		});
	}
};

/**
 * Load rentListing and append to req.
 */
const rentListingById = async (req, res, next, id) => {
	try {
		let rentListing = await RentListing.findById(id);
		if (!rentListing)
			return res.status('400').json({
				error: 'RentListing not found'
			});
		req.rentListing = rentListing;
		next();
	} catch (err) {
		return res.status('400').json({
			error: 'Could not retrieve rentListing'
		});
	}
};

const read = (req, res) => {
	// req.profile.hashed_password = undefined;
	// req.profile.salt = undefined;
	return res.json(req.rentListing);
};

//read all listing
const list = async (req, res) => {
	try {
		let rentListings = await RentListing.find();
		res.json(rentListings);
	} catch (err) {
		return res.status(400).json({
			error: errorHandler.getErrorMessage(err)
		});
	}
};

const update = async (req, res) => {
	try {
		let rentListing = req.rentListing;
		rentListing = _.extend(rentListing, req.body);
		rentListing.updated = Date.now();
		await rentListing.save();
		// rentListing.hashed_password = undefined;
		// rentListing.salt = undefined;
		res.json(rentListing);
	} catch (err) {
		return res.status(400).json({
			error: errorHandler.getErrorMessage(err)
		});
	}
};

const remove = async (req, res) => {
	try {
		let rentListing = req.rentListing;
		let deletedRentListing = await rentListing.remove();
		// deletedRentListing.hashed_password = undefined;
		// deletedRentListing.salt = undefined;
		res.json(deletedRentListing);
	} catch (err) {
		return res.status(400).json({
			error: errorHandler.getErrorMessage(err)
		});
	}
};

//Load all Rent Listings Set by a user
const rentListingsByUserId = async (req, res) => {
	// console.log(req.profile);
	try {
		let rentListing = await RentListing.find({ renter: req.profile._id }).exec();
		res.json(rentListing);
	} catch (e) {
		return res.status(400).json({
			err: errorHandler.getErrorMessage(e)
		});
	}
};

//show interest to a rental post
const showInterest = async (req, res) => {
	try {
		let rentListing = await RentListing.findByIdAndUpdate(req.rentListing._id, { $push: { interested_people: req.auth._id } }, { new: true });
		res.json(rentListing);
	} catch (e) {
		return res.status(400).json({
			err: errorHandler.getErrorMessage(e)
		});
	}
};

//general search
const generalSearch = async (req, res) => {
	let searchTerm = req.body.searchTerm.toString();
	console.log(searchTerm);
	try {
		// let rentListing = await RentListing.find({
		// 	status: true,
		// 	$or: [
		// 		{
		// 			description: {
		// 				$regex: searchTerm,
		// 				$options: 'i'
		// 			}
		// 		},
		// 		{
		// 			title: {
		// 				$regex: searchTerm,
		// 				$options: 'i'
		// 			}
		// 		},
		// 		{
		// 			category: {
		// 				$regex: searchTerm,
		// 				$options: 'i'
		// 			}
		// 		},
		// 		{
		// 			short_address: {
		// 				$regex: searchTerm,
		// 				$options: 'i'
		// 			}
		// 		}
		// 	]
		// });

		await RentListing.init(); //wait for text index to be created

		let rentListing = await RentListing.find({ $text: { $search: searchTerm } });

		res.json(rentListing);
	} catch (e) {
		console.log(e);
		return res.status(400).json({
			err: errorHandler.getErrorMessage(e)
		});
	}
};

// specific search
const specificSearch = async (req, res) => {
	/* let rate_monthly = {
		$gte: 25,
		$lte: 1000000
	};
	let category = 'flat';
	let short_address = 'uttara';
	let description = 'fRESH'.toLowerCase();
	let title = 'aparTMent'.toLowerCase(); */
	//the above fields are for testing for now

	let query = {
		status: true
	};

	// req.body = JSON.parse(req.body);

	if (req.body.category) {
		query.category = req.body.category;
	}
	if (req.body.short_address) {
		query.short_address = req.body.short_address.toLowerCase();
	}
	if (req.body.description) {
		query.description = { $regex: req.body.description.toLowerCase(), $options: 'i' };
	}
	if (req.body.title) {
		query.title = {
			$regex: req.body.title.toLowerCase(),
			$options: 'i'
		};
	}
	if (req.body.rate_monthly) {
		query.rate_monthly = req.body.rate_monthly;
	}

	/* 
	query.category = req.body.category ? req.body.category : undefined;
	query.short_address = req.body.short_address ? req.body.short_address : undefined;
	query.description = req.body.description ? req.body.description : undefined;
	query.title = {
		$regex: req.body.title,
		$options: 'i'
	}
		? req.body.title
		: undefined;
	query.rate_monthly = { $regex: req.body.description, $options: 'i' } ? req.body.rate_monthly : undefined;
 */

	try {
		// console.log(query);
		let rentListing = await RentListing.find(query);
		res.json(rentListing);
	} catch (e) {
		return res.status(400).json({
			err: errorHandler.getErrorMessage(e)
		});
	}
};

//get distinct short_address
const getDistinctShort_address = async (req, res) => {
	try {
		let rentListing = await RentListing.distinct('short_address');
		res.json(rentListing.sort());
	} catch (e) {
		return res.status(400).json({
			err: errorHandler.getErrorMessage(e)
		});
	}
};

//read user profile including all listnig made by him/her
const readUserProfile = async () => {};

export default {
	create,
	rentListingById,
	read,
	list,
	remove,
	update,
	isRenter,
	rentListingsByUserId,
	showInterest,
	generalSearch,
	specificSearch,
	getDistinctShort_address
};
