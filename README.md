# Rental WEB Platform

#### What you need to run this code

1. Node (12.11.1)
2. NPM (6.11.3)
3. MongoDB (4.2.0)

#### How to run this code

1. Make sure MongoDB is running on your system
2. Clone this repository
3. Open command line in the cloned folder,
   - To install dependencies, run `npm install`
   - To run the application for development, run `npm run development`
4. Open [localhost:3000](http://localhost:3000/) in the browser

---
